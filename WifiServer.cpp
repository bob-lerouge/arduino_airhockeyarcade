/**
 * @file WifiServer.cpp
 * @brief The implementation  of class WifiServer.
 * @author Robin Duprat<robin.duprat@epita.fr>
 * @date 2017.09
 */

#include "WifiServer.h"

WifiServer::WifiServer(HardwareSerial &HWSerial, Serial_ &DBGSerial, \
                       String ssid, String pass, String addr, int port) 
  : HWSerial(&HWSerial),
    DBGSerial(&DBGSerial),
    baud(baud),
    ssid(ssid),
    pass(pass),
    addr(addr),
    port(port)
{
}

void WifiServer::setup(void)
{
  HWSerial->flush();
  HWSerial->print("+++"); // To ensure we exit the transparent mode
  delay(100);
  sendCmd("AT");
  sendCmd("AT+RST"); // ESP RESET
  sendCmd("", "ready", 6);
  sendCmd("AT+GMR", "OK", 5);
  getAndCheckMAC(5);
  sendCmd("AT+CWQAP", "OK", 4);  // Should Timeout if already 'setdown'
  sendCmd("AT+CWMODE=2"); // Ask for Access Point mode 'setup'

  // Setup AP (ssid, pass, chanel, ...)
  sendCmd("AT+CWSAP=\"" + ssid + "\",\"" + pass + "\",5,3", "OK", 6);
  
  sendCmd("AT+CIPMUX=0"); // Single connexion alowed / No mux
  sendCmd("AT+CIPMODE=1"); // Set transparent mode

  // Setup Server (address, port)
  sendCmd("AT+CIPSTART=\"UDP\",\"" + addr + "\"," + port + ",2222,0", "OK", 6);
  sendCmd("AT+CIPSEND", ">");
}

void WifiServer::sendCmd(String cmd, String endChar, int timeout_s)
{
  DBGSerial->println("SEND > \"" + cmd + "\"");
  HWSerial->println(cmd);
  if (timeout_s)
    checkSend(endChar, timeout_s * 1000); //Convert in MS
  delay(250);
}

bool WifiServer::checkSend(String endChar, int timeout_ms)
{
  int start_time = millis();
  String data = "";
  while(!data.endsWith(endChar)) {
    if (millis() - start_time > timeout_ms) {
      DBGSerial->println("ERROR > Timout ! (" + endChar + " not found)");
      return false;
    }
    if (!HWSerial->available())
      continue;
    data += (char)HWSerial->read();
  }

  DBGSerial->println("GET  < \"" + endChar + "\"");
  HWSerial->flush();
  return true;
}

String WifiServer::getData(String start, String end, int sz)
{
  int index_s = 0;
  int index_e = 0;
  int packetsize = 0;
  char data = 0;
  
  if (!HWSerial->available())
    return "";
  
  if ((data = (char)HWSerial->read()) < 0)
    return "";
  
  buff += data;

  // Looking for 'start' synchronisation and transmission tag (encapsulation)
  index_s = buff.indexOf(start);
  packetsize = start.length()  + end.length() + sz;
  if (index_s < 0) {
    if (buff.length() > packetsize)
      buff = buff.substring(packetsize - start.length());
    return "";
  }

  index_e = buff.indexOf(end);
  if (index_e < 0) {
    // if end not found, clear buff if size is over the required packetsize
    if (buff.length() >= index_s + packetsize) { 
      DBGSerial->println("ERROR > UDP Packet : Not valid  !!");
      buff = "";
    }
    return "";
  }
  
  if (index_s >= index_e) {
    buff = buff.substring(index_s);
    return "";
  } 
  /* 
  DBGSerial->println((String)"[" + micros() + \
                     (String)"] DURTY DEBUG > GET UDP PACKET (OK) : " + buff);
  */

  // Return everything found between 'start' and 'end' tag
  buff = buff.substring(index_s + start.length(), index_e);
  return buff;
}


String WifiServer::getAndCheckMAC(int timeout_s)
{
  String data = "";
  int MACindex = 0;
  int checkpoint = 0;
  int start_time = millis();

  sendCmd("AT+CIPSTAMAC?", "", 0);
  do {
    if (!HWSerial->available())
      continue;
    data += (char)HWSerial->read();

    if (checkpoint < 1) { // Wait for start of Mac Address <=> ":"
      checkpoint = (data.endsWith(":")) ? 1 : 0;
      MACindex++;
      continue;
    }

    if (checkpoint < 2) { // Wait the end of MAC address
      checkpoint = (data.endsWith("\r")) ? 2 : 1;
      continue;
    }

    if (checkpoint < 3) { // Looking for the ack <=> "OK"
      checkpoint = (data.endsWith("OK")) ? 3 : 2;
      continue;
    }

    HWSerial->flush();
    break; // End
  } while ((millis() - start_time) <= timeout_s * 1000);

  if(checkpoint < 3) { // Timeout ?
    DBGSerial->println("ERROR > Timeout (cannot get MAC)");
    return "";
  }
  
  data.toUpperCase();
  data = data.substring(MACindex,MACindex + 19);
  DBGSerial->println("MAC  > " + data);
  return data;
}
