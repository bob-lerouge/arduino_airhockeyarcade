==========================================================================
PROJECT :
==========================================================================
Name : AIR HOCKEY ARCADE
Code : Arduino
Target : Leonardo Board

Compile stat :
--------------
- Le croquis utilise 14458 octets (50%) de l'espace de stockage de
programmes. Le maximum est de 28672 octets.
- Les variables globales utilisent 817 octets (31%) de mémoire dynamique,
ce qui laisse 1743 octets pour les variables locales. Le maximum est de
2560 octets.

==========================================================================
ADDITION LIBRARY (VIA ARDUINO IDE) :
==========================================================================

TimerOne :
----------
- web : http://playground.arduino.cc/Code/Timer1
- install :
  1) Download TimerOne-r11.zip and extract it in your ~/Arduinlo/libraries/
  2) IDE -> Sketch -> Include library using IDE manager

  /!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\
  ========================================================================
  IMPORTANT : Uncomment line 159 in TimerOne.cpp:start()
  ------------------------------------------------------------------------
  159   TIMSK1 = _BV(TOIE1);          // sets the timer overflow interrupt
  ------------------------------------------------------------------------
  ========================================================================
  ^
  └── Tested on Leonardo with TimerTest.ino with the r11 version (last
      update : Modiied 7:26 PM Sunday, October 09, 2011 by Lex Talionis).

      The timer interrupt is never call after start(), even if the counter
      is clear (TCNT1 = 0) and protected against phantom interrupt.

      Weird fact, the resume() function worked on the board, letting the
      counter continue (without having cleaned it) HOWEVER resume() is
      used by start() after clearing and before checking of TCNT1
      incrementation by phantom protection !

      In my case, it is necessary to set the timer overflow interrupt to
      ensure all next timer interruptions uncommenting line 159.

==========================================================================
FILES :
==========================================================================
.
├── a4988.cpp 	        --> Lib Stepper code
├── a4988.h		--> Lib Stepper header (included and updated)
├── AirHockeyArcade.h	--> Arduino Main header : config with #define
├── AirHockeyArcade.ino	--> Arduino Main code : setup() and loop()
├── Movements.ino	--> Arduino code : setup and contrôle motors part
├── README.txt		--> You are currently reading this file.
├── Tests.ino		--> Arduino code : test for motors
├── TimerTest/		--> Timer1 test arduino research project using 
|                           TimerOne.cpp with uncommented line 159. 
├── WifiServer.cpp	--> C++ implémentation of WifiServer class
└── WifiServer.h	--> Definition of the WifiServer class (included)


