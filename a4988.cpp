/*
  a4988.cpp - - Arduino library for using the a4988 stepper driver
  William Smith, 2014

  ============================================================================
  UPDATE N°1
  ----------

  Add parrallelisme control of N motors.
  1) Setting the number of step for each motor (independently)
  2) Execute the number of step to do (with the same delay :/) for all motors

  Possible update : timer (separate delay).

  UPDATE N°2
  ----------
  Enable motors after setup() call. enable() is still public.

  UPDATE N°3
  ----------
  Timer1 available for Step generation, much more accurate and independant.

  System              | precision                      | Waiting
  --------------------------------------------------------------
  DelayMicrosecond()  | ~ 5 us                         | Active
  --------------------------------------------------------------
  Timer1              | ~ 62,5 ns (on a 16MHz Arduino) | Passive

  /!\ PLEASE SEE README.txt FOR INSTALLATION /!\

  --
  Robin Duprat <robin.duprat@epita.fr>, 2017
  ============================================================================


  => https://github.com/frostybeard/a4988_stepper_library

  The A4988 stepper driver is for Pololu stepper driver boards
  and compatible clones. These boards use the Allegro a4988
  stepper motor driver IC. (see Allegro website for datasheet)

  This library diverges from others that are around, in that it
  assumes that the MS1, MS2, and MS3 pins are connected to gpio
  pins on the Arduino, allowing control over the microstepping
  modes.

  The A4988 is capable of microstepping down to 1/16 of a step,
  enabling fine control over the stepper motor. This fine control
  can be used in, among other things, 3D printers.

  This library provides an interface for setting the different
  step modes, going from full step down to 1/16 step, using a
  simple setter function, where the argument is 1,2,4,8, or 16.


     MS1   MS2   MS3
    -----------------
     low   low   low   Full step
     high  low   low   Half step
     low   high  low   1/4 step
     high  high  low   1/8 step
     high  high  high  1/16 step

  Note:
  Lower delay values can be used in the microstepping mode.
  Values as low as 25 usec can be used in the 1/16 mode
  with some motors.

 */

#include "Arduino.h"
#include "a4988.h"
#include "TimerOne.h"

/*===========================================================================
  == Timer1 interuption handler
  ===========================================================================*/

void timer1Handler()
{
  a4988::Instance().stepOnceTIM();
  //Serial.println("+"); // DURTY DEBUG
  return;
}

/*===========================================================================
  == TIMER STEP : Step methode called by the Timer1
  ===========================================================================*/

void a4988::stepOnceTIM()
{
  long j = 0;

  if (this->timer_on == 1)
  {
    Serial.println("DURTY DEBUG > Timer1 start : "+String(micros()) +" micros"); 
    this->timer_on = 2;
  }

  for (int i = 0; i < N; i++)
  {
    if (!this->is_config[i] || !this->step_todo[i])
      continue;
    digitalWrite(this->step_pin[i], HIGH);
    j++;
  }

  if (!j)
  {
    j = micros();
    j = 2 * j - micros(); // <=> j - (micros() - j)
    Timer1.stop();
    this->timer_on = (timer_on) ? 1 : 0;
    Serial.println("DURTY DEBUG > Timer1 End : " + String(j) + " micros");
    return;
  }

  // Wait for step pulse
  __asm__ __volatile__ (
      "nop" "\n\t"
      "nop" "\n\t"
      "nop" "\n\t"
      "nop" "\n\t"
      "nop" "\n\t"
      "nop" "\n\t"
      "nop" "\n\t"
      "nop" "\n\t"
      "nop" "\n\t"
      "nop" "\n\t"
      "nop" "\n\t"
      "nop");

  for (int i = 0; i < N; i++)
  {
    if (!this->is_config[i] || !this->step_todo[i])
      continue;
    digitalWrite(this->step_pin[i], LOW);
    this->step_todo[i]--;
  }
}

/*===========================================================================
  == CPU STEP : Step only once using delayMicrosecond()
  ===========================================================================*/

void a4988::stepOnceCPU(unsigned long motors[])
{
  for (int i = 0; i < N; i++)
  {
    if (!motors[i] || !this->is_config[i])
      continue;
    digitalWrite(this->step_pin[i], HIGH);
  }
  delayMicroseconds(this->step_delay);

  for (int i = 0; i < N; i++)
  {
    if (!motors[i] || !this->is_config[i])
      continue;
    digitalWrite(this->step_pin[i], LOW);
  }
  delayMicroseconds(this->step_delay);
}

/*===========================================================================
  == Set number of step TO DO
  ===========================================================================*/

void a4988::setStep(unsigned int motor, unsigned long num_steps)
{
  if (motor >= N || !this->is_config[motor])
    return;
  
  this->step_todo[motor] = num_steps;

}

/*===========================================================================
  == Execute the number of previously given steps (TO DO)
  ===========================================================================*/

void a4988::step(void)
{
  long tmp;
  unsigned long motors[N] = {0};
  int save_delay = this->step_delay;
  static unsigned long currentMillis = 0;
  static unsigned long previousMillis = 0;
  int continue_ramp = 0;

  if (this->timer_on)
  {
    step_delay += 30;
    Timer1.setPeriod(this->step_delay);
    tmp = micros();
    tmp = 2 * tmp - micros();
    Serial.println("DURTY DEBUG > Timer 1 start : " + String(tmp) + " micros");
  }
    
  currentMillis = millis();
  while(true)
  {
    

    previousMillis = millis();
    if (previousMillis - currentMillis > 12)
    {
      currentMillis = previousMillis;
      if (this->timer_on == 2 && this->step_delay > 70)
      {
        this->step_delay--;
        Timer1.setPeriod(this->step_delay);
      }
      else if (this->step_delay > 5)
        this->step_delay--;
    }
 
    if (this->timer_on == 2) 
      continue;
    else if (this->timer_on == 1)
      break;

    tmp = 0;
    for(int i = 0; i < N; i++)
    {
      motors[i] = (this->step_todo[i] > 0) ? this->step_todo[i]-- : 0;
      if (motors[i] == 0)
      {
        if (this->step_delay < 28 && !continue_ramp)
          this->step_delay = 28;
        else
          continue_ramp = 1;
      }
          
      tmp += motors[i];
    }
    if (!tmp)
      break;

    stepOnceCPU(motors);
  }

  this->step_delay = save_delay;
}

/*===========================================================================
  == Ramp TEST reducing delay (<=> rise speed) 50us to 1us (CPU/TIM version)
  ===========================================================================*/

void a4988::speedTest(unsigned int motor, unsigned int start_delay, \
                      unsigned int end_delay)
{
  long save_delay = this->step_delay;
  long previousMillis = 0;
  unsigned long currentMillis = 0;
  unsigned long motors[N] = { 0 };

  this->step_delay = start_delay;

  if (this->timer_on)
  {
    this->step_todo[motor] = 50000000;
    Timer1.setPeriod(start_delay);
  }

  while(true)
  {

    if (this->timer_on)
      this->step_todo[motor] = 50000000;

    currentMillis = millis();
    if(currentMillis - previousMillis > 12 && true) // 1/2 sec. | true: ramp
    {
      previousMillis = currentMillis;
      if (this->step_delay > end_delay)
      {
        this->step_delay--;
        if (this->timer_on)
          Timer1.setPeriod(this->step_delay);
        Serial.print("DURTY DEBUG > Interval = ");
        Serial.println(this->step_delay);
      }
      else
      {
        static long _l = 0;
        if (_l == 100)
        {
          this->step_todo[motor] = 0;
          _l = 0;
          break;
        }

        _l++;
      }
    }
    
    if (this->timer_on)
      continue;
    
    motors[motor] = 1;
    stepOnceCPU(motors);
  }

  this->step_delay = save_delay;
}

/*===========================================================================
  == a4988 Class Constructor (Singleton => 1 loading)
  ===========================================================================*/

a4988::a4988(void)
{
  Serial.println("a4988 loaded"); // DURTY DBG
}

/*===========================================================================
  == Used to set up pins and initialize data
  ===========================================================================*/

void a4988::setup(unsigned int motor, int motor_steps, int ms1_pin, \
                  int ms2_pin, int ms3_pin, int dir_pin, int enable_pin, \
                  int step_pin)
{
  static bool first = true;
  if (motor >= N)
    return;

  this->enable_pin[motor] = enable_pin;
  this->dir_pin[motor] = dir_pin;
  this->ms1_pin[motor] = ms1_pin;
  this->ms2_pin[motor] = ms2_pin;
  this->ms3_pin[motor] = ms3_pin;
  this->step_pin[motor] = step_pin;

  // setup the pins on the microcontroller:
  pinMode(this->ms1_pin[motor], OUTPUT);
  pinMode(this->ms3_pin[motor], OUTPUT);
  pinMode(this->ms3_pin[motor], OUTPUT);
  pinMode(this->dir_pin[motor], OUTPUT);
  pinMode(this->enable_pin[motor], OUTPUT);
  pinMode(this->step_pin[motor], OUTPUT);

  if(motor_steps != 0)
  {
    this->motor_steps[motor] = motor_steps;
  }
  else
  {
    this->motor_steps[motor] = 200;       // a common value for steppers
  }

  // use setDelay to change before stepping, otherwise default
  this->step_delay = 20000;         // 20000 us (20 ms) as a default
  this->is_config[motor] = 1;
  this->step_todo[motor] = 0;

  // Enable current motor
  enable(motor, 0);

  // Initialize and Stop Timer by default
  if (!first)
    return;
  first = false;
  Timer1.initialize();
  Timer1.stop();
  Timer1.attachInterrupt(timer1Handler);
  Serial.println("DURTY DEBUG > Timer1 initialized");
}

/*===========================================================================
  == /!\  0: enable  1: disable  /!\
  ===========================================================================*/

void a4988::enable(unsigned int motor, int enable)
{
  if (motor >= N || !this->is_config[motor])
    return;

  digitalWrite(this->enable_pin[motor], enable);  // set enable pin on/off
}

/*===========================================================================
  == /!\  2:running 1: enable  0: disable  /!\ NOT FOLLOW old lib convention
  ===========================================================================*/

void a4988::enableTimer(int timer_on)
{
  if (timer_on < 0 || timer_on > 2) // 
    return;

  this->timer_on =  timer_on;
}


/*===========================================================================
  == Set delay in microseconds, set before starting to step
  ===========================================================================*/

void a4988::setDelay(unsigned long delay)
{
  this->step_delay = delay;
}

/*===========================================================================
  == Set direction: 0 or 1
  ===========================================================================*/

void a4988::setDirection(unsigned int motor, int direction)
{
  if (motor >= N || !this->is_config[motor])
    return;

  if(direction == 0)
  {
    digitalWrite(dir_pin[motor],HIGH);
  }
  else
  {
    digitalWrite(dir_pin[motor],LOW);
  }
}

/*===========================================================================
  == Set microstepping mode : 1, 2, 4, 8, or 16
  ===========================================================================*/

void a4988::setStepMode(unsigned int motor, int stepMode)
{
  if (motor >= N || !this->is_config[motor])
    return;

  this->step_mode[motor] = stepMode;
  switch (stepMode)
  {
    // MS1   MS2   MS3
    case 16:    // high  high  high
      digitalWrite(ms1_pin[motor], HIGH);
      digitalWrite(ms2_pin[motor], HIGH);
      digitalWrite(ms3_pin[motor], HIGH);
      break;
    case 8:    // high  high  low
      digitalWrite(ms1_pin[motor], HIGH);
      digitalWrite(ms2_pin[motor], HIGH);
      digitalWrite(ms3_pin[motor], LOW);
      break;
    case 4:    // low   high  low
      digitalWrite(ms1_pin[motor], LOW);
      digitalWrite(ms2_pin[motor], HIGH);
      digitalWrite(ms3_pin[motor], LOW);
      break;
    case 2:    // high  low   low
      digitalWrite(ms1_pin[motor], HIGH);
      digitalWrite(ms2_pin[motor], LOW);
      digitalWrite(ms3_pin[motor], LOW);
      break;
    case 1:    // low   low   low
      digitalWrite(ms1_pin[motor], LOW);
      digitalWrite(ms2_pin[motor], LOW);
      digitalWrite(ms3_pin[motor], LOW);
      break;
  }
}
