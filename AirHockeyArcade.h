#ifndef AirHockeyArcade_h
#define AirHockeyArcade_h

#include "a4988.h"
#include "WifiServer.h"
#include "TimerOne.h"

#define VERSION 0.51

#define DIRPIN1 8
#define STEPPIN1 7
#define ENABLEPIN1 4

#define DIRPIN2 5
#define STEPPIN2 12
#define ENABLEPIN2 4

#define MOTOR_STEPS 200 // Common value for stepper

#define X_MIN 0
#define X_MAX 380
#define Y_MIN 0
#define Y_MAX 250

#define WIFI_SSID "AirHockeyArcade" // Access Point Name
#define WIFI_PASS "ABCD1234"        // Access Point Password
#define WIFI_IPADDR "192.168.4.2"   // Address IP of the client
#define WIFI_PORT 2223              // Port of the client

#define ROT_COEF  0.70710678118 // 45° => cos(45) = sin(45) = sqrt(2) / 2

int pos_x = 0; // default position
int pos_y = 0; // default position
int target_x = 0; //default target
int target_y = 0; //default target

int pos_unit = 10;  // 10° rotate <=> unité x / y axes (Approximately)


#endif /* AirHockeyArcade_h */
