/**
 * @file WifiServer.h
 * @brief The definition of class WifiServer.
 * @author Robin Duprat<robin.duprat@epita.fr>
 * @date 2017.09
 */

#pragma once // WIFISERVER_H

#include "Arduino.h"

/**
 * Provide an easy-tu-use way to set a AP Wifi Server (from HardwareSerial).
 */
class WifiServer {
  public:
    /*
     * Constructor.
     *
     * @param HWSerial - an reference of HardwareSerial object.
     * @param DBGSerial - an reference of the Serial to use for debug printing.
     * @param ssid - the disired SSID for the Access Point (AP).
     * @param pass - the disired password for AccessPoint.
     * @param addr - the disired IP address for AccessPoint.
     * @param port - the disired port for AccessPoint.
     */ 
    WifiServer(HardwareSerial &HWSerial, Serial_ &DBGSerial, String ssid, \
               String pass, String addr, int port);
    
    /**
     * Setup the whole Access Point Wifi Server.
     */
    void setup(void);
    
    /**
     * Receive UDP Packet and return all data read inside [start;end] tag
     * - Pos
     * - Target
     * - Movements unit
     * 
     * @param start - the 'start' tag
     * @param end - the 'end' tag
     * @param sz - max size of the data
     *
     * @return the data get between start and end tag if success else nothing 
     */
     String getData(String start, String end, int size);
    
     String buff = "";
 
  private: 
     /**
     * Send a command to the chip.
     * 
     * @param cmd - command.
     * @param endChar - characters indicating the expected answer (default "OK")
     * @param timeout - timeout (in sec.) for the chip answer (default 1s).
     */
    void sendCmd(String cmd, String endChar = "OK", int timeout_s = 1);

    /*
     * Checking the correct understanding of the command.
     *
     * @param endChar - characters indicating the expected answer (default "OK")
     * @param timeout - timeout (in ms) for the chip answer (default 1000ms/1s).
     * @retval true - success.
     * @retval false - failure.
     */
    bool checkSend(String endChar = "OK", int timeout_ms = 1000);
    
    /**
     * Return the MAC address or empty String in case of error.
     * @param timeout - timeout (in sec.) for the chip answer (default 1s).
     */
    String getAndCheckMAC(int timeout_s);
 
 
    HardwareSerial *HWSerial;
    Serial_ *DBGSerial;
    int baud = 0;
    int port = 0;
    String ssid = "";
    String pass = "";
    String addr = "";
};
