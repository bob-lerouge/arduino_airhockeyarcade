/*
  a4988.h - - Arduino library for using the a4988 stepper driver
  William Smith, 2014

  ============================================================================
  UPDATE N°1
  ----------

  Add parrallelisme control of N motors.
  1) Setting the number of step for each motor (independently)
  2) Execute the number of step to do (with the same delay :/) for all motors

  Possible update : timer (separate delay).

  UPDATE N°2
  ----------
  Enable motors after setup() call. enable() is still public.

  UPDATE N°3
  ----------
  Timer1 available for Step generation, much more accurate and independant.

  System              | precision                      | Waiting
  --------------------------------------------------------------
  DelayMicrosecond()  | ~ 5 us                         | Active
  --------------------------------------------------------------
  Timer1              | ~ 62,5 ns (on a 16MHz Arduino) | Passive

  /!\ PLEASE SEE README.txt FOR INSTALLATION /!\

  --
  Robin Duprat <robin.duprat@epita.fr>, 2017
  ============================================================================


  => https://github.com/frostybeard/a4988_stepper_library

  The A4988 stepper driver is for Pololu stepper driver boards
  and compatible clones. These boards use the Allegro a4988
  stepper motor driver IC. (see Allegro website for datasheet)

  This library diverges from others that are around, in that it
  assumes that the MS1, MS2, and MS3 pins are connected to gpio
  pins on the Arduino, allowing control over the microstepping
  modes.

  The A4988 is capable of microstepping down to 1/16 of a step,
  enabling fine control over the stepper motor. This fine control
  can be used in, among other things, 3D printers.

  This library provides an interface for setting the different
  step modes, going from full step down to 1/16 step, using a
  simple setter function, where the argument is 1,2,4,8, or 16.


     MS1   MS2   MS3
    -----------------
     low   low   low   Full step
     high  low   low   Half step
     low   high  low   1/4 step
     high  high  low   1/8 step
     high  high  high  1/16 step

  Note:
  Lower delay values can be used in the microstepping mode.
  Values as low as 25 usec can be used in the 1/16 mode
  with some motors.

*/

#ifndef a4988_h
#define a4988_h

#define N 2 // number of motors

class a4988 
{
  public:
    void setup(unsigned int motor, int motor_steps, int ms1_pin, int ms2_pin, \
               int ms3_pin, int dir_pin, int enable_pin, int step_pin);

    void enable(unsigned int motor, int enable);
    void enableTimer(int timer_on);
    // speed setter method:
    void setDelay(unsigned long delay);   // in microseconds

    // mover method:
    // (1 /) 1, 2, 4, 8, or 16
    void setStepMode(unsigned int motor, int stepMode);
    // Set the direction : 0:forward - 1:backward
    void setDirection(unsigned int motor, int direction);
    // Set step to do
    void setStep(unsigned int motor, unsigned long num_steps);
    // Do step "to do" TIM or CPU
    void step(void);

    // Ramp delay start: 50 to 1 us / -- each 1/2sec
    void speedTest(unsigned int motor, unsigned int start_delay, \
                   unsigned int end_delay);

    // Do not call it */!\* used reserved to timer1Handler()
    void stepOnceTIM();

    // Singleton Declaration
    static a4988& Instance()
    {
      static a4988 theSingleInstance;
      return theSingleInstance;
    }
    
    long new_delay = 0;

  private:
    // Singleton protections
    a4988(void);
    a4988(const a4988& cpya4988);

    void stepOnceCPU(unsigned long motors[]);
    int direction[N];               // Direction of rotation
    int motor_steps[N];             // number of steps motor has per revolution
    unsigned long step_delay;    // delay between steps, in us
    int num_steps[N];               // total number of steps to step
    int step_number[N];             // which step the motor is on
    int step_mode[N];               // which mode 1 / 1, 2, 4, 8, or 16
    int step_pin[N];                // pin which controls stepping

    // control pin numbers:
    int ms1_pin[N];                 // for setting different microstepping modes
    int ms2_pin[N];                 // for setting different microstepping modes
    int ms3_pin[N];                 // for setting different microstepping modes
    int dir_pin[N];
    int enable_pin[N];

    int is_config[N] = { 2, 2 };    // Motor[X] ok ? 1:Yes - 0:No => setup(X,..)
    volatile unsigned long step_todo[N]={0};//Number of step execute next step()

    unsigned int period = 0;
    
    volatile int timer_on = 0; // 0: timer off - 1: timer on 
};

#endif /* A4988_H */
