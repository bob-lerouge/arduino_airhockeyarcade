#include "a4988.h"

/*===========================================================================
  == Print the position (0: current pos - 1: target pos)
  ===========================================================================*/
void printPos(bool target)
{
  if (target)
  {
    Serial.print("(");
    Serial.print(target_x);
    Serial.print(", ");
    Serial.print(target_y);
    Serial.println(")");
  }
  else
  {
    Serial.print("(");
    Serial.print(pos_x);
    Serial.print(", ");
    Serial.print(pos_y);
    Serial.println(")");
  }
}

/*===========================================================================
  == Set the current position p(x,y) bounded
  ===========================================================================*/
void updatePos(int x, int y)
{
  pos_x = constrain(x, X_MIN, X_MAX);
  pos_y = constrain(y, Y_MIN, Y_MAX);

  target_x = pos_x;
  target_y = pos_y;
}

/*===========================================================================
  == Set the target position p(x,y) bounded
  ===========================================================================*/
void updateTarget(int x, int y)
{
  target_x = constrain(x, X_MIN, X_MAX);
  target_y = constrain(y, Y_MIN, Y_MAX);
}

/*===========================================================================
  == Set the degree unit to apply 
  ===========================================================================*/
void setPosUnit(int new_pos_unit)
{
  pos_unit = constrain(new_pos_unit, 0, 360);
}

/*===========================================================================
  == update position moving motors
  ===========================================================================*/

void updateMove()
{
  a4988& motors = a4988::Instance();

  float px = 0;
  float py = 0;
  float tx = 0;
  float ty = 0;

  if (pos_x == target_x && pos_y == target_y)
    return;

  px = ROT_COEF * (pos_x - pos_y);
  py = ROT_COEF * (pos_x + pos_y);
  tx = ROT_COEF * (target_x - target_y);
  ty = ROT_COEF * (target_x + target_y);

  motors.setDirection(0, px > tx);
  // long to avoid overflow
  motors.setStep(0, (1600 * (long)pos_unit * abs(tx - px)) / 360);
  Serial.print("DEBUG > M1 moto step : ");
  Serial.println(abs(tx - px));

  motors.setDirection(1, py > ty);
  // long to avoid overflow
  motors.setStep(1, (1600 * (long)pos_unit * abs(ty - py)) / 360);
  Serial.print("DEBUG > M2 moto step : ");
  Serial.println(abs(ty - py));

  motors.step();

  pos_x = target_x;
  pos_y = target_y;
}

/*===========================================================================
  == Data packet parsing (remote command)
  ===========================================================================*/

void readData(String data)
{
  
  int mc[4];
  String debug;

  a4988& motors = a4988::Instance();

  if (!data.length())
    return;

  if (data.startsWith("su")) { // set unit
    // do not need constrain
    setPosUnit(data.substring(2).toInt());
    return;
  }

  if (data.startsWith("sp")) { // set pos
    // do not need constrain
    updatePos((data.substring(2,5)).toInt(), (data.substring(5,8)).toInt());
    return;
  }
  if (data.startsWith("step0") || (data.startsWith("step1"))) {
    motors.setStep(data[4]-'0',constrain(data.substring(5,10).toInt(),0,99999));
    motors.step();
    return;
  }
  if (data.startsWith("st")) { // set target
    // do not need constrain
    updateTarget((data.substring(2,5)).toInt(), (data.substring(5,8)).toInt());
    return;
  }
  if (data.startsWith("test0") || data.startsWith("test1")) {
    mc[0] = constrain(data.substring(5, 7).toInt(), 0, 99);
    mc[1] = constrain(data.substring(7, 9).toInt(), 0, 99);
    motors.speedTest(data[4]-'0', mc[0], mc[1]);
    Serial.print("DEBUG > Start Test Speed (" + String(mc[0]) + " < delay < ");
    Serial.println(String(mc[1]) + ")");
    return;
  }
  if (data.startsWith("TIM")) {
    Serial.println("DEBUG > Timer Enable");
    motors.enableTimer(1);
    return;
  }
  if (data.startsWith("CPU")) {
    Serial.println("DEBUG > Timer Disable");
    motors.enableTimer(0);
    return;
  }
  if (data.startsWith("mc0") || data.startsWith("mc1") ) { // motor control
    mc[0] = data.substring(3,5).toInt();
    mc[1] = data.substring(5,9).toInt();
    mc[2] = data.substring(9,10).toInt();
    mc[3] = data.substring(10,15).toInt();
    Serial.print("DEBUG > [raw] mc(mode = ");
    Serial.print(mc[0]);
    Serial.print(", delay = ");
    Serial.print(mc[1]);
    Serial.print(", dir = ");
    Serial.print(mc[2]);
    Serial.print(", step = ");
    Serial.print(mc[3]);
    Serial.println(");");
    Serial.print("DEBUG > [constrain] mc(mode = ");
    Serial.print(mc[0]);
    Serial.print(", delay = ");
    Serial.print(constrain(mc[1],30,4000));
    Serial.print(", dir = ");
    Serial.print(constrain(mc[2],0,1));
    Serial.print(", step = ");
    Serial.print(constrain(mc[3],0,99999));
    Serial.println(");");

    motors.setStepMode(data[2] - '0', mc[0]); // do not need constrain
    motors.setDelay(constrain(mc[1],30,4000));
    motors.setDirection(data[2] - '0', constrain(mc[2],0,1));
    motors.setStep(data[2] - '0', 1600 * (long)constrain(mc[3],0,99999) / 360);
    motors.step();
    return;
  }

  Serial.println("DEBUG > undefined UDP packet data : " + data);
}
