#include "a4988.h"

/*
  StepperMotors : DEPRECATED
  ===============
   
  TEST (2 types : both M1 & M2 or single only)
*/


void testMotors(a4988 Motor1, a4988 Motor2)
{
  if (times == 0)
  {
    updateTarget(12,12);
  }
  else if (times == 1)
  {
    delay(1000);
    updateTarget(12,12);
  }
  else if (times == 2)
  {
    delay(1000);
    updateTarget(24,24);
  }
  else if (times == 3)
  {
    delay(1000);
    updateTarget(-12,-12);
  }
  else if (times == 4)
  {
    delay(1000);
    updateTarget(-24,-24);
  }
  else if (times == 5)
  {
    delay(1000);
    updateTarget(12,12);
  }
  else if (times == 6)
  {
    delay(1000);
    updateTarget(0,0);
  }
  
  times++;
}
