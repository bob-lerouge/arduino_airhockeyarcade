#include "Arduino.h"
#include "TimerOne.h"

#define VERSION 0.1

long clk;

/*===========================================================================
  == Timer1 interuption handler
  ===========================================================================*/

void itHandler()
{
  Serial.print("Timer1 itHandler() : ");
  Serial.print(micros());
  Serial.println(" micros");
}

/*===========================================================================
  == SETUP()
  ===========================================================================*/

void setup()
{
  Serial.begin(115200);
  while (!Serial);
  delay(1000); // Wait 1s for user Serial setup
  Serial.println((String)"#### INIT : TIMER1 TEST (AHA) " + VERSION + " ####");

  pinMode(A3, INPUT); // pushbutton TESTSUITE

  Timer1.initialize();
  Timer1.attachInterrupt(itHandler);

  clk = millis();
}

/*===========================================================================
  == LOOP()
  ===========================================================================*/

void loop()
{
  bool timer_on = true;

  while(true)
  {

    if ((long)millis() - clk > 1000) // 1Hz loop (check the button every second)
    {
      if (analogRead(A3) < 100) // pushbutton pushed => RUNNING TESTSUITE
      {
        if(timer_on)
        {
          Serial.println("Timer1 stop()");
          Timer1.stop();
          timer_on = false;

        }
        else
        {
          Serial.println("Timer1 start()");
          Timer1.start();
          timer_on = true;
        }
      }

      clk = millis();
    }
  }
}
