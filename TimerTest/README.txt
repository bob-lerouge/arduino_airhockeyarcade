==========================================================================
PROJECT :
==========================================================================
Name : Timer1 test (library TimerOne.h)
Code : Arduino
Target : Leonardo Board

==========================================================================
RESEARCH :
==========================================================================

Tested on Leonardo with TimerTest.ino with the r11 version (last
update : Modiied 7:26 PM Sunday, October 09, 2011 by Lex Talionis).
Available here : http://playground.arduino.cc/Code/Timer1

Usefull link : http://forum.arduino.cc/index.php?topic=201565.0

The timer interrupt is never call after start(), even if the counter
is clear (TCNT1 = 0) and protected against phantom interrupt.

Weird fact, the resume() function worked on the board, letting the
counter continue (without having cleaned it) HOWEVER resume() is
used by start() after clearing and before checking of TCNT1
incrementation by phantom protection !

In my case, it is necessary to set the timer overflow interrupt to
ensure all next timer interruptions uncommenting line 159 in
TimerOnc.cpp:start()

------------------------------------------------------------------------
159   TIMSK1 = _BV(TOIE1);          // sets the timer overflow interrupt
------------------------------------------------------------------------

==========================================================================
FILES :
==========================================================================
.
├── README.txt			--> You are currently reading this file.
├── TimerOne.cpp         	--> Lib Timer1 code
├── TimerOne.h	         	--> Lib Timer1 header (included)
└── TimerTest.ino         	--> Arduino Main code : setup() and loop()


