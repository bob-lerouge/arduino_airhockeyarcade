#include "AirHockeyArcade.h"

// WifiServer Declaration
WifiServer wifi(Serial1, Serial, WIFI_SSID, WIFI_PASS, WIFI_IPADDR, WIFI_PORT);

bool test_on = 0;
long clk = 0;
int times;

/*===========================================================================
  == Arduino Main SETUP
  ===========================================================================*/

void setup()
{
  Serial.begin(115200);
  Serial1.begin(115200); // for ESP connection
  //while (!Serial);
  delay(3000); // Wait 3s for user Serial setup
  Serial.println((String)"#### INIT : AIR HOCKEY ARCADE " + VERSION + " ####");

  //pinMode(A3, INPUT); // pushbutton testsuite

  // Motors 1 & 2 steup
  a4988& motors = a4988::Instance();
  motors.setup(0, MOTOR_STEPS, A4, A4, A4, DIRPIN1, ENABLEPIN1, STEPPIN1);
  motors.setup(1, MOTOR_STEPS, A4, A4, A4, DIRPIN2, ENABLEPIN2, STEPPIN2);
  motors.setStepMode(0, 1);
  motors.setStepMode(1, 1);
  motors.setDelay(55);

  // AP Wifi Server setup
  wifi.setup();

  test_on = false;
  times = -1;
}

/*===========================================================================
  == Arduino Main LOOP
  ===========================================================================*/

void loop()
{
  String data = "";

  data = wifi.getData("ST", "ED", 16);
  readData(data);
  updateMove();

  // 1s loop for DEBUG
  if ((long)millis() - clk > 1000)
  {
    Serial.println("DEBUG > wifi.buff = " + wifi.buff);
    Serial.print("      > pos (x,y) = ");
    printPos(false);
    Serial.print("      > target (x,y) = ");
    printPos(true);

    clk = millis();
    delay(42);
    /*
      if (analogRead(A3) < 100) // pushbutton pushed => RUNNING TESTSUITE
      {
      test_on = true;
      times = -1; // for motors test cycle count
      }

      if (test_on)
      {
      Serial.println("## RUNNING TESTSUITE ##");
      test_on = false;
      times++;
      }
    */
  }
}
